/// Interact with Owncast instances using API calls.
///
/// wowncast makes API calls to Owncast instances and allows storing the results in cache.
/// It is designed for the wowncast command line interface and is configured through local config files.
/// Owncast project homepage: https://owncast.online
///
/// ```dart
/// var instance = Owncast("https://watch.owncast.online", fromConfig: true);
/// await instance.fetchConfig(updateCache: true);
/// await instance.fetchStatus(updateCache: true);
/// ```
library wowncast;

export 'src/owncast.dart';
export 'src/utils.dart';
export 'src/config.dart';
export 'src/constants.dart' show wowncastVersion;
