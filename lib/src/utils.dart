import 'dart:io';
import 'package:toml/toml.dart';
import 'package:wowncast/wowncast.dart';
import 'cache.dart' as cache;

Future<Map<String, Set<Owncast>>> processAllInstances(
    TomlDocument config) async {
  // Prepare output
  var output = {"live": <Owncast>{}, "offline": <Owncast>{}};

  // Prepare futures
  List<Future> futures = <Future>[];

  // For each instance found in config
  for (var instanceURL in config.toMap()['instances']) {
    // Create an Owncast instance
    final instance = Owncast(instanceURL, fromCache: true);

    // Add future
    futures.add(fetchAllData(instance));
  }

  // Run all futures
  var processedInstances = await Future.wait(futures);

  // For each instance found in config
  for (var instance in processedInstances) {
    // Make sure data was correctly fetched
    if (instance.config == null || instance.status == null) {
      continue;
    }

    // Separate instance by online status
    if (instance.status!["online"]) {
      output["live"]!.add(instance);
    } else {
      output["offline"]!.add(instance);
    }
  }

  // Update the cache
  cache.putLiveInstances(output["live"]);
  cache.putLastUpdated();

  return output;
}

Future<Owncast> fetchAllData(Owncast instance) async {
  // Try and fetch the API data
  try {
    await instance.fetchConfig(updateCache: true);
    await instance.fetchStatus(updateCache: true);
  } catch (e) {
    print("Error: could not reach ${instance.url}");
  }

  return instance;
}

String? findSystemHome() {
  // Return the path to the home directory if present on the platform
  return Platform.environment['HOME'] ?? Platform.environment['USERPROFILE'];
}

String slugifyUrl(String url) {
  return url.replaceAll(RegExp(r"[:/?&\.]"), "_");
}
