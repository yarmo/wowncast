/// Current wowncast version
const wowncastVersion = "0.2.0";

/// The Owncast API config endpoint
const endpointApiConfig = "/api/config";

/// The Owncast API status endpoint
const endpointApiStatus = "/api/status";

/// The default config location
const defaultConfigPath = "~/.config/wowncast/";

/// The default config file location
const defaultConfigFilename = "config.toml";

/// The default cache location
const defaultCachePath = "~/.cache/wowncast/";

/// The default filename of the cached live streams
const defaultCacheLiveFilename = "live_streams";

/// The default filename of the cached last updated time
const defaultCacheLastUpdatedFilename = "last_updated";

/// The minimum time in seconds between instance requests (serve cache instead)
const timeoutDataFetchSeconds = 60;

/// The default config file content
const defaultConfigFileContent = """player = 'mpv'
instances = [
]
""";
