import 'dart:io';
import 'dart:convert';
import 'package:toml/toml.dart';
import 'package:http/http.dart' as http;
import 'package:wowncast/wowncast.dart';
import 'constants.dart' as constants;
import 'cache.dart' as cache;

class Owncast {
  /// The URL of the owncast instance.
  final String url;

  /// The instance's config API response.
  Map config = {};

  /// The instance's status API response.
  Map status = {};

  /// The last time the config API was fetched.
  DateTime configLastUpdated = DateTime.fromMillisecondsSinceEpoch(0);

  /// The last time the config API was fetched.
  DateTime statusLastUpdated = DateTime.fromMillisecondsSinceEpoch(0);

  // Basic constructor
  Owncast(this.url, {bool fromCache = false}) {
    if (fromCache == true) {
      // Get the cache file
      var urlSlug = slugifyUrl(url);
      var cacheFile = File("${cache.getPath()}instance_$urlSlug.toml");
      cacheFile.createSync(recursive: true);

      // Read and parse the cache
      var dataRaw = cacheFile.readAsStringSync();
      if (dataRaw.isNotEmpty) {
        var dataToml = TomlDocument.parse(dataRaw);
        config = jsonDecode(dataToml.toMap()["config"]);
        configLastUpdated =
            dataToml.toMap()["configLastUpdated"].toUtcDateTime();
        status = jsonDecode(dataToml.toMap()["status"]);
        statusLastUpdated =
            dataToml.toMap()["statusLastUpdated"].toUtcDateTime();
      }
    }
  }

  @override
  int get hashCode => url.hashCode;

  @override
  bool operator ==(Object other) {
    return other is Owncast && url == other.url;
  }

  /// Fetch the instance's config using the API.
  Future<void> fetchConfig({bool updateCache = false}) async {
    // Check whether the data was fetched recently
    var fetchInterval = DateTime.now().difference(statusLastUpdated);
    if (fetchInterval.inSeconds <= constants.timeoutDataFetchSeconds) {
      return;
    }

    // Make the HTTP GET request
    final urlApi = Uri.parse('$url${constants.endpointApiConfig}');
    final http.Response response;
    try {
      response = await http.get(urlApi).timeout(Duration(seconds: 5));
    } catch (e) {
      throw 'API endpoint could not be reached';
    }

    // Store the response
    configLastUpdated = DateTime.now();
    config = jsonDecode(response.body);

    // Update the cache
    if (updateCache) {
      putInCache();
    }
  }

  /// Fetch the instance's status using the API.
  Future<void> fetchStatus({bool updateCache = false}) async {
    // Check whether the data was fetched recently
    var fetchInterval = DateTime.now().difference(statusLastUpdated);
    if (fetchInterval.inSeconds <= constants.timeoutDataFetchSeconds) {
      return;
    }

    // Make the HTTP GET request
    final urlApi = Uri.parse('$url${constants.endpointApiStatus}');
    final http.Response response;
    try {
      response = await http.get(urlApi).timeout(Duration(seconds: 5));
    } catch (e) {
      throw 'API endpoint could not be reached';
    }

    // Store the response
    statusLastUpdated = DateTime.now();
    status = jsonDecode(response.body);

    // Update the cache
    if (updateCache) {
      putInCache();
    }
  }

  /// Stores the instance's data in cache.
  void putInCache() {
    // Get the cache file
    var urlSlug = slugifyUrl(url);
    var cacheFile = File("${cache.getPath()}instance_$urlSlug.toml");
    cacheFile.createSync(recursive: true);

    // Prepare the data to be cached
    var data = {
      "url": url,
      "config": jsonEncode(config).toString(),
      "configLastUpdated": configLastUpdated,
      "status": jsonEncode(status).toString(),
      "statusLastUpdated": statusLastUpdated
    };
    var dataToml = TomlDocument.fromMap(data);

    cacheFile.writeAsStringSync(dataToml.toString());
  }
}
