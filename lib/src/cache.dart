import 'dart:io';
import 'owncast.dart';
import 'utils.dart';
import 'constants.dart' as constants;

/// Gets the path to the cache location.
///
/// It uses the [constants.defaultCachePath] path.
/// It calls [utils.findSystemHome] to attempt to interpolate the "~" symbol.
String getPath() {
  // Get the default cache location
  var cachePath = constants.defaultCachePath;

  // Find the home location based on the platform
  var home = findSystemHome();

  // Replace the home location if present
  if (home != null) {
    cachePath = cachePath.replaceAll("~", home);
  }

  return cachePath;
}

/// Gets the list of Owncast instances that are currently streaming.
Set<Owncast> getLiveInstances() {
  // Load and return the cache file
  var cacheFile = File("${getPath()}${constants.defaultCacheLiveFilename}");
  cacheFile.createSync(recursive: true);

  // Iterate over the lines of the cache file
  Set<Owncast> instances = <Owncast>{};
  for (var url in cacheFile.readAsLinesSync()) {
    instances.add(Owncast(url));
  }

  return instances;
}

/// Stores the list of Owncast instances that are currently streaming
void putLiveInstances(Set<Owncast>? instances) {
  // Handle null input
  instances = instances ?? <Owncast>{};

  // Generate the cache content
  var cacheContent = "";
  for (var instance in instances) {
    cacheContent += "${instance.url}\n";
  }

  // Find the home location based on the platform
  var cacheFile = File("${getPath()}${constants.defaultCacheLiveFilename}");
  cacheFile.createSync(recursive: true);
  cacheFile.writeAsStringSync(cacheContent);
}

/// Adds a single Owncast instance to the live streams cache.
///
/// If the instance is already on the list, this function does nothing.
void addInstanceToLiveInstances(Owncast instance) {
  // Load from cache
  var liveInstances = getLiveInstances();

  // Add the instance
  var wasAdded = liveInstances.add(instance);

  // Store to cache
  if (wasAdded) {
    putLiveInstances(liveInstances);
  }
}

/// Adds a single Owncast instance to the live streams cache.
///
/// If the instance is not on the list, this function does nothing.
void removeInstanceFromLiveInstances(Owncast instance) {
  // Load from cache
  var liveInstances = getLiveInstances();

  // Remove the instance
  var wasRemoved = liveInstances.remove(instance);

  // Store to cache
  if (wasRemoved) {
    putLiveInstances(liveInstances);
  }
}

/// Gets the DateTime when the last status check was performed
DateTime getLastUpdated() {
  // Find the home location based on the platform
  var cacheFile =
      File("${getPath()}${constants.defaultCacheLastUpdatedFilename}");
  cacheFile.createSync(recursive: true);
  return DateTime.parse(cacheFile.readAsStringSync());
}

/// Stores the current DateTime check to cache
void putLastUpdated() {
  // Find the home location based on the platform
  var cacheFile =
      File("${getPath()}${constants.defaultCacheLastUpdatedFilename}");
  cacheFile.createSync(recursive: true);
  cacheFile.writeAsStringSync("${DateTime.now().toIso8601String()}\n");
}
