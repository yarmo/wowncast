build :
	mkdir -p ./dist
	dart pub get
	dart compile exe ./bin/wowncast.dart -o ./dist/wowncast

clean :
	rm ./dist/wowncast
	rmdir ./dist

install :
	sudo install -D -m755 ./dist/wowncast /usr/bin/wowncast
	sudo install -D -m644 ./assets/wowncast-completion.bash /usr/share/bash-completion/completions/wowncast
