# wowncast

Owncast terminal client

**Project in beta**: expect bugs! Do [raise an issue](https://codeberg.org/yarmo/mowoo/issues) or send a message to the `#wowncast:irc.libera.chat` IRC channel.

## Features

- Manage Owncast subscriptions from the terminal
- Check which Owncast subscriptions are currently streaming
- Directly open the stream in your preferred media player or in the browser

Tested on Linux but should work on Windows and macOS as well.

## Getting started

### Installation

#### Using pre-compiled binaries

The project has a pre-compiled linux/amd64 binary for immediate download in the [Releases section](https://codeberg.org/yarmo/wowncast/releases).

For other OS or architecture, compile from source.

#### Compiling from source

Run the following commands (assuming you have [dart](https://dart.dev) installed already):

```bash
git clone https://codeberg.org/yarmo/wowncast.git
cd wowncast
dart pub get
dart compile exe bin/wowncast.dart -o wowncast
```

You now have a binary named `wowncast`, which you could either run directly or move into a specialized folder:

```bash
# On linux
sudo mv wowncast /usr/bin/wowncast
```

#### Bash tab completion

To enable bash tab completion, run:

```bash
sudo cp assets/wowncast-completion.bash /usr/share/bash-completion/completions/wowncast
```

Please note that due to a quirk of bash tab completion, it only works when using quotation marks:

```bash
# This will show suggestions :)
wowncast watch "https://
```

```bash
# This will not show suggestions :(
wowncast watch https://
```

### Integrate with rofi (linux)

`wowcast` integrates nicely with the application launcher `rofi` by following these steps.

Write the script `~/.local/bin/rofi_wowncast.sh`:

```bash
#!/bin/bash

if [[ $1 ]]; then
	url=$(echo $1 | awk -F'(' '{print $NF}' | sed '$ s/.$//')
	~/.local/bin/wowncast watch $url > /dev/null &
else
	~/.local/bin/wowncast check -l | sed 's/  //;'
fi
```

## Usage

Add a new Owncast to follow:

```bash
wowncast add https://watch.owncast.online
```

Check which Owncast instances are currently streaming:

```bash
wowncast check
```

Watch an Owncast instance in the configured media player (`mpv` by default):

```bash
wowncast watch https://watch.owncast.online
```

Watch an Owncast instance in the preferred browser:

```bash
wowncast browser https://watch.owncast.online
```

To get help:

```bash
wowncast --help
```

To modify the configuration, edit the file at `~/.config/wowncast/config.toml`.

A config file can look somewhat like this:

```toml
player = 'mpv'
instances = [
	'https://watch.owncast.online'
]
```

To use a different configuration file, specify it using the `-c` option.

## Additional information

The wowncast project is licensed under GPL-3.0-or-later.

wowncast is largely inspired by [wtwitch](https://github.com/krathalan/wtwitch).

wowncast is not associated with the [Owncast project](https://owncast.online).

All contributions are welcome: PRs, bug reports, feature suggestions :)