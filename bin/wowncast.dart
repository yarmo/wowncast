import 'dart:io';
import 'package:toml/toml.dart';
import 'package:args/args.dart';
import 'package:tint/tint.dart';
import 'package:wowncast/wowncast.dart';

Future<void> main(List<String> arguments) async {
  // Init
  exitCode = 0;

  // Parse arguments
  var parser = ArgParser();
  parser.addFlag('help', abbr: 'h');
  parser.addFlag('live-only', abbr: 'l');
  parser.addFlag('minimal');
  parser.addOption("config", abbr: "c");
  parser.addCommand('help');
  parser.addCommand('list');
  parser.addCommand('check');
  parser.addCommand('add');
  parser.addCommand('remove');
  parser.addCommand('watch');
  parser.addCommand('browser');
  var argResults = parser.parse(arguments);

  if (argResults["help"] == true) {
    await helpCommand(full: true);
    return;
  }

  if (argResults.command == null) {
    await helpCommand(full: false);
    return;
  }

  // Load the config
  final TomlDocument config;
  try {
    config = getConfig(argResults["config"]);
  } catch (e) {
    print("Error: could not load config");
    exit(2);
  }

  // Try executing the command
  try {
    switch (argResults.command!.name) {
      case "help":
        await helpCommand(full: true);
        break;
      case "list":
        await listCommand(config, minimal: argResults["minimal"]);
        break;
      case "check":
        await checkCommand(config, liveOnly: argResults["live-only"]);
        break;
      case "add":
        await addCommand(config, argResults.command!.rest);
        break;
      case "remove":
        await removeCommand(config, argResults.command!.rest);
        break;
      case "watch":
        await watchCommand(config, argResults.command!.rest);
        break;
      case "browser":
        await browserCommand(argResults.command!.rest);
        break;
      default:
    }
  } catch (e) {
    print("Error: $e");
    exitCode = 2;
  }

  // Exit the process
  exit(exitCode);
}

Future<void> listCommand(TomlDocument config, {bool minimal = false}) async {
  // Get the instances from the config
  var instances = config.toMap()["instances"];

  // Display the instances
  instances.sort();
  if (!minimal) {
    print("ALL INSTANCES".bold());
  }
  for (var instance in instances) {
    if (minimal) {
      print("$instance");
    } else {
      print("  $instance");
    }
  }
}

Future<void> checkCommand(TomlDocument config, {bool liveOnly = false}) async {
  // Process all the instances
  var data = await processAllInstances(config);

  // Iterate over all the live instances
  var liveInstances = data["live"];
  if (liveInstances != null && liveInstances.isEmpty == false) {
    if (!liveOnly) {
      print("LIVE STREAMS".bold());
    }

    // For each instance, print a line
    for (var instance in liveInstances) {
      print(
          "  ${instance.config["name"]}: ${instance.status["streamTitle"]} (${instance.url})");
    }
  }

  if (liveOnly) {
    return;
  }

  // Iterate over all the offline instances
  var offlineInstances = data["offline"];
  if (offlineInstances != null && offlineInstances.isEmpty == false) {
    print("OFFLINE".bold());

    // For each instance
    for (var instance in offlineInstances) {
      if (instance.status["lastDisconnectTime"] == null) {
        // Print a simple line
        print("  ${instance.url}");
      } else {
        // Print a line with "last seen online"
        var lastSeenOnline =
            DateTime.parse(instance.status["lastDisconnectTime"]);
        print(
            "  ${instance.url}: last seen online ${lastSeenOnline.year}-${lastSeenOnline.month}-${lastSeenOnline.day}");
      }
    }
  }
}

Future<void> addCommand(TomlDocument config, List<String> instanceUrls) async {
  // Handle input
  if (instanceUrls.isEmpty) {
    throw "Specify an instance to watch";
  }
  if (instanceUrls.length > 1) {
    throw "Only specify a single instance to watch";
  }

  // Check if the instance is already in the config
  if (config.toMap()["instances"].contains(instanceUrls[0])) {
    print("Instance ${instanceUrls[0]} is already on the subscription list");
    return;
  }

  // Update the config and write it
  var newConfig = config.toMap();
  newConfig["instances"].add(instanceUrls[0]);
  writeConfig(newConfig);

  // Provide feedback
  print("Instance ${instanceUrls[0]} was added to the subscription list");
}

Future<void> removeCommand(
    TomlDocument config, List<String> instanceUrls) async {
  // Handle input
  if (instanceUrls.isEmpty) {
    throw "Specify an instance to watch";
  }
  if (instanceUrls.length > 1) {
    throw "Only specify a single instance to watch";
  }

  // Check if the instance is already in the config
  if (!config.toMap()["instances"].contains(instanceUrls[0])) {
    print("Instance ${instanceUrls[0]} is not on the subscription list");
    return;
  }

  // Update the config and write it
  var newConfig = config.toMap();
  newConfig["instances"].remove(instanceUrls[0]);
  writeConfig(newConfig);

  // Provide feedback
  print("Instance ${instanceUrls[0]} was removed from the subscription list");
}

Future<void> watchCommand(
    TomlDocument config, List<String> instanceUrls) async {
  // Handle input
  if (instanceUrls.isEmpty) {
    throw "Specify an instance to watch";
  }
  if (instanceUrls.length > 1) {
    throw "Only specify a single instance to watch";
  }

  // Run the command
  await Process.run(config.toMap()["player"], [instanceUrls[0]]);
}

Future<void> browserCommand(List<String> instanceUrls) async {
  // Handle input
  if (instanceUrls.isEmpty) {
    throw "Specify an instance to watch";
  }
  if (instanceUrls.length > 1) {
    throw "Only specify a single instance to watch";
  }

  // Run the command
  switch (Platform.operatingSystem) {
    case "linux":
      await Process.run("xdg-open", [instanceUrls[0]]);
      break;
    case "macos":
      await Process.run("open", [instanceUrls[0]]);
      break;
    case "windows":
      await Process.run("explorer", [instanceUrls[0]]);
      break;
    default:
      break;
  }
}

Future<void> helpCommand({bool full = false}) async {
  // Generate the concise help output
  var content = """
wowncast - Owncast terminal client

""";

  content += "USAGE\n".bold();
  content += """  \$ wowncast check
  \$ wowncast add <URL>
  \$ wowncast watch <URL>

""";

  if (!full) {
    content += "For a more detailed help, run wowncast --help";
    print(content);
    return;
  }

  // Extend the help output
  if (full) {
    content += "OPTIONS\n".bold();
    content += """
  -h, --help        show help
  -c, --config      set the config file path (default: ~/.config/wowncast/config.toml)
  -l, --live-only   display only instances currently streaming (where applicable)

""";

    content += "COMMANDS\n".bold();
    content += """
  help              show help
  list              list all the instances on the subscription list
  check             check which instances are currently streaming
  add <URL>         add the instance at the URL to the subscription list
  remove <URL>      remove the instance at the URL from the subscription list
  watch <URL>       open the URL in the configured media player
  browser <URL>     open the URL in the default browser

""";
  }

  content += "ADDITIONAL INFORMATION\n".bold();
  content += """
  Version: $wowncastVersion
  License: GPL-3.0-or-later
  To find more information or file a bug, please refer to source code repository:
    https://codeberg.org/yarmo/wowncast
""";

  print(content);
}
