#!/usr/bin/env bash
#
# Bash autocompletion file for wowncast
#
# Based on the autocompletion file of wtwitch
#   https://github.com/krathalan/wtwitch/blob/27aef67601b264183317578f4fca791325c8dc04/src/wtwitch-completion.bash

_wowncast_completions()
{
  # COMP_WORDS[0] = wowncast
  # COMP_WORDS[1] = list, add, watch, help, etc.
  
  # TODO: Fix colons and bash autocompletion
  # local cur, prev
  # _get_comp_words_by_ref -n : cur prev words cword

  if [[ "${#COMP_WORDS[@]}" -lt "3" ]]; then
    # Return the commands
    mapfile -t COMPREPLY <<< "$(compgen -W "list add remove check watch browser help" "${COMP_WORDS[1]}")"
  elif [[ "${#COMP_WORDS[@]}" == "3" ]]; then
    if [[ "${COMP_WORDS[1]}" == "watch" || "${COMP_WORDS[1]}" == "browser" ]]; then
      # Get cached live streams
      _live_streams_file_text="$(<"${HOME}/.cache/wowncast/live_streams")"

      # Transform text to indexed array
      mapfile -t _live_streams <<< "${_live_streams_file_text}"

      # Return list of live streams
      mapfile -t COMPREPLY <<< "$(compgen -W "${_live_streams[*]}" "${COMP_WORDS[2]}")"
    elif [[ "${COMP_WORDS[1]}" == "remove" ]]; then
      # Get all subscribed instances
      mapfile -t _subscriptions <<< "$(wowncast list --minimal)"
      
      # Return the instances
      mapfile -t COMPREPLY <<< "$(compgen -W "${_subscriptions[*]}" "${COMP_WORDS[2]}")"
    fi
  fi
}

complete -F _wowncast_completions wowncast
